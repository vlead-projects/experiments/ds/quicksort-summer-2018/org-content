#+TITLE: Quick sort Experiment
#+AUTHOR: Daksh Lalwani and surendra
#+DATE: [2018-06-18 Mon]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil
* Repository Structure
#+BEGIN_EXAMPLE

content/
|--- README.org
|--- init.sh
|--- makefile
|--- make_config.sample
|--- make_congif.py
|--- src/
     |--- index.org
     |--- exp-cnt/
     |--- docs/
     |--- story-board/

#+END_EXAMPLE
