#+TITLE: Experiment Content
#+AUTHOR: VLEAD
#+DATE: [2018-05-15 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: pmbl lu task txta
#+EXCLUDE_TAGS:
#+OPTIONS: ^:nil' prop:t


* Introduction
Contains the exeriment content.
|--------+-----------------------------+-------------|
| *SNo.* | *Purpose*                   | *Link*      |
|--------+-----------------------------+-------------|
|     1. | Contains Experiment content | [[./concrete.org][Exp Content]] |
|--------+-----------------------------+-------------|
