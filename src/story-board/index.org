#+TITLE: Storyboard Content
#+AUTHOR: VLEAD
#+DATE: [2018-05-15 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: pmbl lu task txta
#+EXCLUDE_TAGS:
#+OPTIONS: ^:nil' prop:t


* Introduction
Contains the storyboard content of our project.
|--------+-----------------------------+--------------------|
| *SNo.* | *Purpose*                   | *Link*             |
|--------+-----------------------------+--------------------|
|     1. | Contains Storyboard content | [[./concrete.org][Storyboard Content]] |
|--------+-----------------------------+--------------------|
